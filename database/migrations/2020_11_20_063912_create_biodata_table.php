<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata', function (Blueprint $table) {
            $table->id();
            $table->string('posisi');
            $table->string('nama');
            $table->integer('noktp');
            $table->string('TTL');
            $table->string('jenis_kelamin');
            $table->string('agama');
            $table->string('goldar');
            $table->string('status');
            $table->text('alamatKTP');
            $table->text('alamat_tinggal');
            $table->string('email');
            $table->string('notelp');
            $table->string('panggilan_darurat');
            $table->text('skill');
            $table->string('penempatan');
            $table->string('harapan_gaji');
            $table->string('ttd');
            // $table->foreignId('id_pendidikan')->constrained()->onDelete('cascade')->onUpdate('cascade');
            // $table->foreignId('id_pelatihan')->constrained()->onDelete('cascade')->onUpdate('cascade');
            // $table->foreignId('id_pekerjaan')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}
