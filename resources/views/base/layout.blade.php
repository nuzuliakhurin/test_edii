<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    @include('base.header')
    @include('base.navbar')
    @yield('content')
    @include('base.footer')
    @yield('scripts', '')
  </body>
</html>
