@extends('base.layout')

@section('title', 'Biodata')

@section('biodata')
  active
@stop

@section('content')
<div class="content">
      <div class="container-fluid">
        <div class="row">
          @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <i class="material-icons">close</i>
            </button>
            <span>
              <b> Success - </b> {{session()->get('message')}}</span>
          </div>
          @endif
          @if($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <i class="material-icons">close</i>
            </button>
            <span>
              <b> Error - </b> {{$errors->first()}}</span>
          </div>
          @endif
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Isikan biodata Anda</h4>

              </div>
              <div class="card-body">
                <form action="{{ route('biodata.add') }}" method="post">
                  @csrf
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">POSISI YANG DILAMAR</label>
                        <input type="text" class="form-control" name="posisi">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">NAMA</label>
                        <input type="text" class="form-control" name="nama">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">NO. KTP</label>
                        <input type="text" class="form-control" name="noktp">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">TEMPAT, TANGGAL LAHIR</label>
                        <input type="text" class="form-control" name="TTL">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">JENIS KELAMIN</label>
                        <select class="form-control" name="jenis_kelamin">
                          <option value="Perempuan">Perempuan</option>
                          <option value="Laki-laki">Laki-Laki</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">AGAMA</label>
                        <input type="text" class="form-control" name="agama">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">GOLONGAN DARAH</label>
                        <input type="text" class="form-control" name="goldar">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">STATUS</label>
                        <input type="text" class="form-control" name="status">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">ALAMAT KTP</label>
                        <input type="text" class="form-control" name="alamatKTP">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">ALAMAT TINGGAL</label>
                        <input type="text" class="form-control" name="alamat_tinggal">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">EMAIL</label>
                        <input type="text" class="form-control" name="email">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">NO. TELP</label>
                        <input type="text" class="form-control" name="notelp">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">ORANG YANG DAPAT DIHUBUNGI</label>
                        <input type="text" class="form-control" name="panggilan_darurat">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">PENGHASILAN YANG DIHARAPKAN (/bulan)</label>
                        <input type="text" class="form-control" name="harapan_gaji">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">BERSEDIA DITEMPATKAN DISELURUH KANTOR PERUSAHAAN (ya/tidak)</label>
                        <select class="form-control" name="penempatan">
                          <option value="Ya">Ya</option>
                          <option value="Tidak">Tidak</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>SKILL *tuliskan keahlian & keterampilan yang saat ini anda miliki.</label>

                        <textarea class="form-control" rows="5" name="skill"></textarea>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Send </button>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop
