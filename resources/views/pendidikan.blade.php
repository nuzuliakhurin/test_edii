@extends('base.layout')

@section('title', 'Biodata')

@section('biodata')
  active
@stop

@section('content')
<div class="content">
      <div class="container-fluid">
        <div class="row">
          @if(session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <i class="material-icons">close</i>
            </button>
            <span>
              <b> Success - </b> {{session()->get('message')}}</span>
          </div>
          @endif
          @if($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <i class="material-icons">close</i>
            </button>
            <span>
              <b> Error - </b> {{$errors->first()}}</span>
          </div>
          @endif
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Isikan biodata Anda</h4>

              </div>
              <div class="card-body">
                <form action="{{ route('pendidikan.add') }}" method="post">
                  @csrf
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">JENJANG PENDIDIKAN TERAKHIR</label>
                        <input type="text" class="form-control" name="jenjang">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">NAMA INSTITUSI AKADEMIK</label>
                        <input type="text" class="form-control" name="nama">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">JURUSAN</label>
                        <input type="text" class="form-control" name="jurusan">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">TAHUN LULUS</label>
                        <input type="text" class="form-control" name="tahun_lulus">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">IPK</label>
                        <input type="text" class="form-control" name="IPK">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>SKILL *tuliskan keahlian & keterampilan yang saat ini anda miliki.</label>

                        <textarea class="form-control" rows="5" name="skill"></textarea>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Send </button>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop
