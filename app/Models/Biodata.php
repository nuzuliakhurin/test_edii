<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    use HasFactory;

    protected $table = 'biodata';
    protected $fillable = ['posisi', 'nama', 'noktp', 'TTL', 'jenis_kelamin', 'agama', 'goldar', 'status', 'alamatKTP', 'alamat_tinggal', 'email', 'notelp', 'panggilan_darurat', 'skill', 'penempatan', 'harapan_gaji'];
}
