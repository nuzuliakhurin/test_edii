<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biodata;
use DB;

class BiodataController extends Controller
{
    public function index()
    {
      return view('biodata');
    }

    protected function validateRequest()
    {
      return request()->validate([
        'posisi' => 'required',
        'nama' => 'required',
        'noktp' => 'required',
        'TTL' => 'required',
        'jenis_kelamin' => 'required',
        'agama' => 'required',
        'goldar' => 'required',
        'status' => 'required',
        'alamatKTP' => 'required',
        'alamat_tinggal' => 'required',
        'email' => 'required',
        'notelp' => 'required',
        'panggilan_darurat' => 'required',
        'skill' => 'required',
        'penempatan' => 'required',
        'harapan_gaji' => 'required'
      ]);
    }

    public function add(Request $request)
    {
      $data = $this->validateRequest();

      $biodata = Biodata::create($data);

      return route('biodata')->with('message', 'Sukses menambahkan biodata');
    }
}
